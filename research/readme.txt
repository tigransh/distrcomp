1. Abstract
There are huge amounts of open data available in the internet descibed as interrelated conceptual graphs. RDF and Linked Data are one of approaches and standards now widely accepted and used in different areas. 
Using this huge amounts of data is sometimes a challenge. There is query language(SPARQL) to retrieve the data, but the amount of retrieved data is sometimes too big for human readibility and usage.  
Often people look for homogeneous interrelated information, i.e. , list of records about concepts of the same time satisfiying some criteria, e.g. list of places, persons, protein, etc. In such cases PageRank algorithm can be useful for ordering the retrieved data by the importance of the interrelations inside the graph or retrieved data.
The goal of the project is to apply PageRank angorithm implemented in Hadoop on 
data of interrelated homogenous nodes, i.e. a graph of nodes having same type. DBPedia provides a good source of knowledge described in RDF language which can be retrieves and later processed by Hadoop.
2. Getting data from DBPedia.
We need to retrieve a set of interconnected nodes(graph) of the same type in order to run PageRank correctly.
DBPedia has an online interface for querying its knowledge base with SPAQRL query languag( http://dbpedia.org/sparql).
The following examples retrieves all Person pair URIs(Unified Resource Identifier) having relation ,ordered by first person's uri:

PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX dbprop: <http://dbpedia.org/property/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT ?person1,?person2 WHERE {
 
 ?person1 a dbo:Person.
 ?person1 dbo:relation ?person2.
 ?person1 dbo:birthPlace ?place1.
 ?person2 dbo:birthPLace ?place2.
  FILTER(?place1!=?place2)
 
  }
ORDER BY ASC(?person1)

The retrieved data can be saved in different formats. CSV is a good choice for later uploading into HDFS and processing with Hadoop.

