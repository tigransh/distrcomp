package objects;

import java.io.Serializable;

public class Product implements Serializable{

	
	private static final long serialVersionUID = -1790057305553710648L;
	
	private String name;
	
	public String getName() {
		return name;
	}
	
	public Product(String name) {
		this.name=name;
	}
	
	public String toString() {
		return name;
	}
	

}
