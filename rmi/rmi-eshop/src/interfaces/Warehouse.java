package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;


import objects.Product;



public interface Warehouse extends Remote{
	int getAvailableNumber(Product product) throws RemoteException;
	int deliverProduct(Product product,int number) throws RemoteException;

}
