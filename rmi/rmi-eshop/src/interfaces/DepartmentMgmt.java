package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import objects.Department;

public interface DepartmentMgmt extends Remote{
	 List<Department> getDepartments() throws RemoteException;
	 

}
