package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import objects.Department;
import objects.Product;

public interface ProductMgmt extends Remote{
	List<Product> getProducts(Department department) throws RemoteException;
	int getPrice(Product product) throws RemoteException;
	boolean buyProduct(Product product,int number) throws RemoteException;
}
