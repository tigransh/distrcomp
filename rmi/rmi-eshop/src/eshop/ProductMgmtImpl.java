package eshop;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import interfaces.ProductMgmt;
import objects.Department;
import objects.Product;

public class ProductMgmtImpl implements ProductMgmt{
	private WarehouseClient warehouse;
	
	private Map<String,String> productDepts;
	private Map<String,Integer> productPrices;
	private Map<String,Integer> productQuantity;
	
	public ProductMgmtImpl() {
		productDepts=new HashMap<>();
		productPrices=new HashMap<>();
		
		productDepts.put("mixer","electronics");
		productDepts.put("hair dryer","electronics");
		productDepts.put("desktop","computers");
		productDepts.put("laptop","computers");
		productDepts.put("mp3 player","gadgets");
		productDepts.put("ipod","gadgets");
		
		productPrices.put("mixer",15);
		productPrices.put("hair dryer",20);
		productPrices.put("desktop",500);
		productPrices.put("laptop",1000);
		productPrices.put("mp3 player",25);
		productPrices.put("ipod",50);
		
		productQuantity.put("mixer",0);
		productQuantity.put("hair dryer",0);
		productQuantity.put("desktop",0);
		productQuantity.put("laptop",0);
		productQuantity.put("mp3 player",0);
		productQuantity.put("ipod",0);
		
		warehouse=new WarehouseClient();
		
	}
	
	

	@Override
	public List<Product> getProducts(Department department) throws RemoteException {
		List<Product> products=new ArrayList<>();
		if(department!=null&&department.getName()!=null && productDepts.containsValue(department.getName())) {
			for(String productName:productDepts.keySet()) {
				if(department.getName().equals(productDepts.get(productName))) {
					products.add(new Product(productName));
				}
			}
		}		
		return products;
	}

	@Override
	public int getPrice(Product product) throws RemoteException {
		return product!=null&&product.getName()!=null?
				productPrices.get(product.getName()):-1;
		
	}

	@Override
	public boolean buyProduct(Product product, int number) throws RemoteException {
		boolean ret=false;
		if(product!=null&&product.getName()!=null&&productQuantity.containsKey(product.getName())) {
			synchronized(productQuantity) {
				
				int quantity=productQuantity.get(product.getName());
				if(quantity>=number) {
					productQuantity.put(product.getName(), quantity-number);
					ret=true;
				}
				else {
					int wsNum=warehouse.getAvailableNumber(product);
					if(wsNum>0) {
						//deliver 10 more if available
						int delivered=warehouse.deliverProduct(product, number-quantity+10);
						int newNum=delivered+productQuantity.get(product.getName());
						if(newNum>=number) {
							productQuantity.put(product.getName(), newNum-number);
							ret=true;
						}
						else {
							productQuantity.put(product.getName(), newNum);
							ret=false;
						}
					}
				}
			}
		}
		return ret;
	}
}
