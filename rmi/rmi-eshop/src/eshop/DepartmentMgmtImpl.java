package eshop;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import interfaces.DepartmentMgmt;
import objects.Department;

public class DepartmentMgmtImpl implements DepartmentMgmt{
	private List<Department> departments;
	
	public DepartmentMgmtImpl() {
		initializeDepartments();
	}
	
	private void initializeDepartments() {
		departments=new ArrayList<>();
		departments.add(new Department("electronics"));
		departments.add(new Department("computers"));
		departments.add(new Department("gadgets"));		
	}

	@Override
	public List<Department> getDepartments() throws RemoteException {
		return departments;
	}

}
