package eshop;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import interfaces.Warehouse;
import objects.Product;

public class WarehouseClient implements Warehouse{
	private Warehouse warehouse;
	
	public  WarehouseClient() {
		try {
			Registry registry = LocateRegistry.getRegistry(1098);
			warehouse = (Warehouse) registry.lookup("warehouse");
			System.out.println("warehouse stub received");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int getAvailableNumber(Product product) throws RemoteException {	
		return warehouse.getAvailableNumber(product);
	}

	@Override
	public int deliverProduct(Product product, int number) throws RemoteException {
		return warehouse.deliverProduct(product, number);
	}
	
		
	public static void main(String args[]) {
		WarehouseClient wsClient=new WarehouseClient();
		Product product=new Product("computer");
		try {
			int num=wsClient.getAvailableNumber(product);
			System.out.println(num);
		} catch (RemoteException e) {
			
			e.printStackTrace();
		}
    }

	

}
