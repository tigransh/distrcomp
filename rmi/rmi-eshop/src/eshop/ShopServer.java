package eshop;


import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import interfaces.DepartmentMgmt;
import interfaces.ProductMgmt;

public class ShopServer {
	
	

	public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {

        	Registry registry = LocateRegistry.getRegistry("127.0.0.1",1099);
        	DepartmentMgmt depts = new DepartmentMgmtImpl();
            DepartmentMgmt deptsStub =
                (DepartmentMgmt) UnicastRemoteObject.exportObject(depts,45002);            
            registry.rebind("departments", deptsStub);                   
            System.out.println("departments bound");
            
            ProductMgmt products = new ProductMgmtImpl();
            ProductMgmt productsStub =
                (ProductMgmt) UnicastRemoteObject.exportObject(products,45003);            
            registry.rebind("products", productsStub);                   
            System.out.println("products bound");
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
