package rmi_client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;


import interfaces.DepartmentMgmt;
import interfaces.ProductMgmt;
import objects.Department;
import objects.Product;

public class RmiClient {
	
	public static void getDepartments() {
		try {
			Registry registry = LocateRegistry.getRegistry(1099);
			DepartmentMgmt deptMgmt = (DepartmentMgmt) registry.lookup("departments");
			List<Department> depts= deptMgmt.getDepartments();
			if(depts!=null) {
				for(Department dept:depts) {
					System.out.println("dept: "+dept);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static void getProducts() {
		try {
			Registry registry = LocateRegistry.getRegistry(1099);
			ProductMgmt productMgmt = (ProductMgmt) registry.lookup("products");
			List<Product> products= productMgmt.getProducts();
			if(products!=null) {
				for(Product prod:products) {
					System.out.println("prod: "+prod);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	public static void main(String args[]) {		
		getDepartments();
		getProducts();
    }   
}
