package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import objects.Product;

public interface ProductMgmt extends Remote{
	List<Product> getProducts() throws RemoteException;

}
