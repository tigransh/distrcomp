package objects;

import java.io.Serializable;

public class Department implements Serializable{
	private String name;
	
	private static final long serialVersionUID = 8906671964872175706L;
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}

}
