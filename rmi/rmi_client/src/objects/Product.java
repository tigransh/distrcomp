package objects;

import java.io.Serializable;

public class Product implements Serializable{
	private String name;

	
	private static final long serialVersionUID = -1790057305553710648L;
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	

}
