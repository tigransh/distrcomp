package warehouse;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import interfaces.Warehouse;
import objects.Product;

public class WarehouseImpl implements Warehouse{
	Map<String,Integer> inventory;
	
	public WarehouseImpl() {
		initializeInventory();
	}
	
	private void initializeInventory() {
		inventory=new HashMap<>();
		inventory.put("laptop", 100);
		inventory.put("iphone", 100);
		
	}
	

	@Override
	public int getAvailableNumber(Product product) throws RemoteException {
		Integer number=null;
		if(product!=null&&product.getName()!=null&&inventory.containsKey(product.getName())) {
			number=inventory.get(product.getName());
		}
		return number!=null?number:0;
	}

	@Override
	public int deliverProduct(Product product, int number) throws RemoteException {
		int delivered=0;
		if(product!=null&&product.getName()!=null&&inventory.containsKey(product.getName())) {
			synchronized(inventory) {
				int available=inventory.get(product.getName());
				delivered=Math.min(number, available);
				inventory.put(product.getName(), available-delivered);
				
			}
		}
		return delivered;
	}

}
