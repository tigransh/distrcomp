package warehouse;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import interfaces.Warehouse;

public class WarehouseServer {
	
	public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {

        	Registry registry = LocateRegistry.getRegistry("127.0.0.1",1098);
        	Warehouse ws = new WarehouseImpl();

            Warehouse wsStub =
                (Warehouse) UnicastRemoteObject.exportObject(ws,46002);            
            registry.rebind("warehouse", wsStub);                   
            System.out.println("warehouse bound");
            
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
