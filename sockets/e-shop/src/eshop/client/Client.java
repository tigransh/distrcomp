package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import common.Constants;

public class Client {

	public static void send(String message) {
		try (Socket socket = new Socket(Constants.HOST, Constants.PORT);
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		) {

			String inputLine, outputLine;
			inputLine = message;
			out.println(inputLine);
			out.flush();

			while ((outputLine = in.readLine()) != null) {
				System.out.println(outputLine);
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void typing() {

		try (BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));) {
			while (true) {
				String inputLine = stdIn.readLine();

				if (inputLine.equals("exit;")) {
					break;
				} else {
					send(inputLine);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Closing interface...");

	}

	public static void main(String[] args) {
		typing();
	}

}
