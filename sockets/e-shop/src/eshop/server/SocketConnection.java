package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocketConnection implements Runnable {
	private final static Logger LOGGER=LoggerFactory.getLogger(SocketConnection.class);

	

	private Socket socket;
	private MessageProcessor mp;




	SocketConnection(Socket socket,MessageProcessor mp) {
		this.socket=socket;
		this.mp=mp;
		
	}

	@Override
	public void run() {
//		System.out.println("socketconnection> Started");
		LOGGER.debug("socketconnection> Started");
		
		try(
				PrintWriter out=new PrintWriter(socket.getOutputStream());
				BufferedReader in=new BufferedReader(
						new InputStreamReader(socket.getInputStream()));
				)
		
		{
			
					
			String message=in.readLine();
//			System.out.println("message="+message);
			String[] response=mp.process(message);
			if(response!=null) {
				for(String responseLine:response) {
					out.println(responseLine);
				}
			}
			out.flush();
			socket.close();
			
	} catch (IOException e) {
		LOGGER.error("",e);
	}
	}


}