package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executors;

import common.Constants;

public class Server {

	public static int numberOfOpenConnections;

	public static String[] depts = new String[] { "electronics", "computers", "gadgets" };
	public static String[][] products = new String[][] { { "mixer", "hair dryer" }, { "desktop", "laptop" },
			{ "mp3 player", "ipod" } };
	public static int[] prices = new int[] { 20, 10, 500, 1000, 50, 100 };

	public static final void main(String[] args) {

		try (ServerSocket serverSocket = new ServerSocket(Constants.PORT);

		) {
			System.out.println("Server started....");
			numberOfOpenConnections = 0;
			while (true) {
				if (numberOfOpenConnections < Constants.MAX_NUMBER_OF_CONNECTIONS) {
					final Socket clientSocket = serverSocket.accept();
					numberOfOpenConnections++;
					SocketConnection conn = new SocketConnection(clientSocket, new MessageProcessor());
					Thread t = new Thread(conn);
					t.start();

				}

			}

		} catch (IOException e) {
			e.printStackTrace();

		}

	}

}
