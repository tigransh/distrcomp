package server;

import java.util.HashMap;
import java.util.Map;

public class MessageProcessor {

	public static String[] commands = new String[] { "?", "list depts", "list products in <dept>", "price <product>",
			"buy <product>", "exit;" };
	public static String[] depts = new String[] { "electronics", "computers", "gadgets" };
	public static String[][] products = new String[][] { { "mixer", "hair dryer" }, { "desktop", "laptop" },
			{ "mp3 player", "ipod" } };
	public static Map<String, String> productPrices = new HashMap<String, String>() {
		{
			put("mixer", "20");
			put("hair dryer", "10");
			put("desktop", "500");
			put("laptop", "1000");
			put("mp3 player", "50");
			put("ipod", "100");
		};
	};
	public static int[] prices = new int[] { 20, 10, 500, 1000, 50, 100 };

	public String[] process(String message) {
		String[] ret = null;
		if (message.endsWith("exit;")) {
			System.out.println("Connection closed!");
			ret = new String[] { "Connection closed!" };

		} else if (message.equals("?")) {
			ret = commands;
		} else if (message.equals("list depts")) {
			ret = depts;
		} else if (message.startsWith("list products in")) {
			int dept = -1;
			if (message.endsWith("electronics")) {
				dept = 0;
			} else if (message.endsWith("computers")) {
				dept = 1;
			} else if (message.endsWith("gadgets")) {
				dept = 2;
			}
			if (dept > -1) {
				ret = products[dept];
			} else {
				ret = new String[] { "unknown dept" };
			}
		} else if (message.startsWith("price")) {
			String prodName = message.substring(5).trim();
			ret = new String[] { productPrices.get(prodName) };

		} else if (message.startsWith("buy")) {
			String prodName = message.substring(3).trim();

			ret = new String[] { prodName + " bought for " + productPrices.get(prodName) + "$" };

		} else {
			ret = new String[] { "unknown command" };
		}

		return ret;

	}

}
