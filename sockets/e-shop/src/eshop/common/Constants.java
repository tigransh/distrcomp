package common;

public class Constants {
	
	public static final String HOST="localhost";
	public static final int PORT=3333;
	public static final int MAX_NUMBER_OF_CONNECTIONS=1000;

}
