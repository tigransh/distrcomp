# README #

This is the repository for practical works of Distributed Computing course of Europeran Academy's Postgraduate students.

### What is this repository for? ###

* The repository contains practical projects of the course.
* Version 1.0

 

### How do I get set up? ###

* Setup and configuration details are provided inside each project.


### Contribution guidelines ###
* The students will be provided access to use and modify the projects.


### Who do I talk to? ###
* Tigran.Shahinyan@gmail.com

