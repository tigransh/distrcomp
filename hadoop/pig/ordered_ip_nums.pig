records = LOAD 'data/input/nasa' using PigStorage(' ');
grouped_records = group records by $0;
nums = foreach grouped_records generate group, COUNT(records.$0);
ordered_nums = order nums by $1 asc;
dump ordered_nums;

