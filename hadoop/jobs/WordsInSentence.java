import java.io.IOException;

public class WordsInSentence {

	public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		// private Text out=new Text();
		private Text word = new Text();
		// private Text word2 = new Text();

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			StringTokenizer itr = new StringTokenizer(value.toString(), ".");
			while (itr.hasMoreTokens()) {
				String sentence = itr.nextToken();

				String[] words = sentence.split(" ");
				Set<String> uniqueWords = new HashSet<>();
				uniqueWords.addAll(Arrays.asList(words));

				for (String currentWord : uniqueWords) {
					currentWord=currentWord.toLowerCase().trim();
					if(!currentWord.isEmpty()) {
						for (String currentAssoc : uniqueWords) {
							currentAssoc=currentAssoc.toLowerCase().trim();
							if (!currentWord.equals(currentAssoc)&&!currentAssoc.isEmpty()) {
								word.set(currentWord + "->" + currentAssoc);
								context.write(word, one);
							}
						}
					}
				}

				// word.set(itr.nextToken());
				// context.write(word, one);
			}
		}
	}

	public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "word associations in sentences");
		job.setJarByClass(WordsInSentence.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
