import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Life {

	public static final int ARRAY_SIZE = 3;

	public static class CellWritable implements WritableComparable {
		private IntWritable row;
		private IntWritable column;

		public CellWritable() {
			this.setRow(0);
			this.setColumn(0);
		}

		public CellWritable(int row, int column) {
			this.setRow(row);
			this.setColumn(column);
		}

		void setRow(int num) {
			this.row = new IntWritable(num);
		}

		void setColumn(int num) {
			this.column = new IntWritable(num);
		}

		public int getRow() {
			return row.get();
		}

		public int getColumn() {
			return column.get();
		}

		@Override
		public void readFields(DataInput in) throws IOException {

			row.readFields(in);
			column.readFields(in);

		}

		@Override
		public void write(DataOutput out) throws IOException {

			row.write(out);
			column.write(out);
		}

		public String toString() {
			return row.toString() + " " + column.toString() + " ";
		}

		@Override
		public int compareTo(Object c) {
			CellWritable that = (CellWritable) c;
			if (c != null) {
				if (this.getRow() > that.getRow()) {
					return 1;
				} else if (this.getRow() < that.getRow()) {
					return -1;
				} else {
					if (this.getColumn() > that.getColumn()) {
						return 1;
					} else if (this.getColumn() < that.getColumn()) {
						return -1;
					} else
						return 0;
				}
			}
			return 1;
		}

	}

	public static class TokenizerMapper extends Mapper<Object, Text, CellWritable, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		// private Text word = new Text();
		private CellWritable cell = new CellWritable();

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			String[] a1 = line.split(" ");
			if (a1.length == 3) {
				int row=Integer.parseInt(a1[0]);
				int col=Integer.parseInt(a1[1]);
				int alive=Integer.parseInt(a1[2]);
				if(alive!=0) {
					List<int[]> neighbors=getNeighbors(row,col);
					for(int[] neighbor:neighbors) {
						cell.setRow(neighbor[0]);
						cell.setColumn(neighbor[1]);
						context.write(cell, one);
					}
					
				}
				
				int lineNum = Integer.parseInt(a1[0]);
				if (lineNum >= 0 && lineNum < ARRAY_SIZE) {
					String content = a1[1];
					char[] ca = content.toCharArray();
					for (int i = 0; i < ca.length && i < ARRAY_SIZE; i++) {
						if (ca[i] == 'x') {
							List<int[]> neighbors = getNeighbors(lineNum, i);
							for (int[] neighbor : neighbors) {
								cell.setRow(neighbor[0]);
								cell.setColumn(neighbor[1]);

								context.write(cell, one);
							}
						}
					}

					// StringTokenizer itr = new StringTokenizer(value.toString());
					// while (itr.hasMoreTokens()) {
					// word.set(itr.nextToken());
					// context.write(word, one);
					// }
				}
			}
		}
	}

	public static class IntSumReducer extends Reducer<CellWritable, IntWritable, CellWritable, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(CellWritable key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}

	public static List<int[]> getNeighbors(int row, int column) {
		System.out.println("getNeighbors called for row=" + row + ",column=" + column);
		List<int[]> neighbors = new ArrayList<>();
		if (row > 0) {
			neighbors.add(new int[] { row - 1, column });
			if (column > 0) {
				neighbors.add(new int[] { row - 1, column - 1 });
			}
			if (column < ARRAY_SIZE - 1) {
				neighbors.add(new int[] { row - 1, column + 1 });
			}

		}
		if (row < ARRAY_SIZE - 1) {
			neighbors.add(new int[] { row + 1, column });
			if (column > 0) {
				neighbors.add(new int[] { row + 1, column - 1 });
			}
			if (column < ARRAY_SIZE - 1) {
				neighbors.add(new int[] { row + 1, column + 1 });
			}
		}
		if (column > 0) {
			neighbors.add(new int[] { row, column - 1 });
		}
		if (column < ARRAY_SIZE - 1) {
			neighbors.add(new int[] { row, column + 1 });
		}

	
		return neighbors;

	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "game of life");
		job.setJarByClass(Life.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(CellWritable.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
